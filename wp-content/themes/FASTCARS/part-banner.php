<?php if ( is_front_page() ) : $style = 'banner_1'; endif; ?>
<?php if ( is_page( array( 'quienes-somos' ) ) ) : $style = 'banner_2'; endif; ?>
<?php if ( is_page( array( 'servicios' ) ) ) : $style = 'banner_3'; endif; ?>
<?php if ( is_page( array( 'parque-automotor' ) ) ) : $style = 'banner_4'; endif; ?>
<?php if ( is_page( array( 'operadores-logisticos' ) ) ) : $style = 'banner_5'; endif; ?>
<?php if ( is_page( array( 'localizacion-satelital' ) ) ) : $style = 'banner_6'; endif; ?>
<?php if ( is_page( array( 'gestion-integral-sst' ) ) ) : $style = 'banner_7'; endif; ?>
<?php if ( is_page( array( 'contacto' ) ) ) : $style = 'banner_8'; endif; ?>
<!-- Begin Banner -->
	<section class="banner <?php echo $style; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'servicios' ) ) ) : dynamic_sidebar( 'banner_servicios' ); endif; ?>
				<?php if ( is_page( array( 'parque-automotor' ) ) ) : dynamic_sidebar( 'banner_parque_automotor' ); endif; ?>
				<?php if ( is_page( array( 'operadores-logisticos' ) ) ) : dynamic_sidebar( 'banner_conductores' ); endif; ?>
				<?php if ( is_page( array( 'localizacion-satelital' ) ) ) : dynamic_sidebar( 'banner_localizacion_satelital' ); endif; ?>
				<?php if ( is_page( array( 'gestion-integral-sst' ) ) ) : dynamic_sidebar( 'banner_gestion_integral_sst' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->