<!-- Begin Block 4 -->
	<section class="block_4 wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-center align-bottom">
			<div class="small-12 medium-8 columns">
				<h3 class="text-center">CLIENTES</h3>
				<?php echo do_shortcode( '[tc-owl-carousel carousel_cat="clientes" order="ASC"]' ); ?>
			</div>
			<div class="small-12 medium-4 columns">
				<?php dynamic_sidebar( 'block_4' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 4 -->